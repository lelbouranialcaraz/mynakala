from flask import request, Flask, jsonify, send_from_directory
import requests
from typing import Dict
from flask_cors import CORS


app = Flask(__name__, static_url_path='', static_folder='front_mynkl/build')

CORS(app)

# "https://apitest.nakala.fr/search?size=2500&fq=created%3D2022"
#  source env/bin/activate


# TODO : gestion des status d erreurs et exception

# @app.errorhandler(Exception)
# def handle_exception(error,status):
#     response = jsonify({"error": str(error)})
#     response.status_code = status
#     return response

@app.route("/", defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')


@app.route('/data_deposited', methods=['POST'])
def post_datas():
    if request.method == 'POST':

        req = request.get_json()
        url_choice: str = req.get('baseurl')
        api_key: str = req.get('api_key')
        current_donnee = req.get('donnee')
        general_datas: list = fetch_nakala_datas(
            f"{url_choice}/search?fq=scope%3Ddata%3Bstatus%3Dpublished&order=relevance&page=1&size=10000", api_key)
        obj_list = []
        for data in general_datas["datas"]:
            data_clean = extract_metadata2(data)
            obj_list.append(data_clean)
        target_obj = extract_metadata2(current_donnee)

        result = recommend_most_similar_objects(
            target_obj, obj_list, general_datas["datas"])
        # if not(interested_data):
        #     return handle_exception("aucune datas de disponibles",500)
        return jsonify(result)


# fetch les collections du user
def fetch_nakala_datas(route: str, api_key: str):
    if ("users" in route):
        response = requests.post(route,
                                 headers={"x-api-key": api_key})
    else:
        response = requests.get(route,
                                headers={"x-api-key": api_key})
    return response.json()


def extract_metadata2(obj: Dict) -> Dict:
    metadata = {}
    metadata['identifier'] = obj['identifier']
    for meta in obj['metas']:
        if meta['propertyUri'] == 'http://nakala.fr/terms#title':
            if 'title' not in metadata:
                metadata['title'] = []
            metadata['title'].append(meta['value'])
        elif meta['propertyUri'] == 'http://purl.org/dc/terms/subject':
            if 'subjects' not in metadata:
                metadata['subjects'] = []
            metadata['subjects'].append(meta['value'])
        elif meta['propertyUri'] == 'http://purl.org/dc/terms/description':
            if 'description' not in metadata:
                metadata['description'] = []
            metadata['description'].append(meta['value'])
    return metadata


def recommend_most_similar_objects(target_obj, object_list, original_ids):
    target_title = set(target_obj.get('title', []))
    target_subjects = set(target_obj.get('subjects', []))
    target_description = set(target_obj.get('description', []))

    similarities = []
    for obj in object_list:
        sim = 0
        if obj['title'] == target_title:
            sim += 1
        if 'subjects' in obj:
            sim += len(target_subjects.intersection(set(obj['subjects'])))
        if 'description' in obj:
            sim += int(obj['description'] == target_description)
        similarities.append((obj, sim))
    similarities.sort(key=lambda x: -x[1])
    recommended_objs = [obj for obj, sim in similarities[:3]]
    target_identifiers = [obj['identifier'] for obj in recommended_objs]
    original_objs = [
        obj for obj in original_ids if obj['identifier'] in target_identifiers]

    return original_objs[:3]


# nltk.download('wordnet')
# # Fonction pour obtenir les synonymes d'un mot à partir de WordNet
# def get_synonyms(word):
#     synonyms = set()
#     for syn in wordnet.synsets(word):
#         for lemma in syn.lemmas():
#             synonyms.add(lemma.name())
#     return synonyms


# # Fonction pour calculer la similarité entre deux documents spaCy en utilisant les vecteurs de mots et les entités nommées
# def get_similarity(doc1, doc2):
#     vector_similarity = doc1.similarity(doc2)
#     named_entity_similarity = doc1.ents == doc2.ents
#     return vector_similarity + named_entity_similarity


# # Fonction pour recommander l'objet le plus similaire et pertinent à partir d'un élément cible CORRECTE
# def recommend_most_similar_objects(target_obj, object_list):
#     target_doc = nlp(' '.join(target_obj['title']) + ' ' + ' '.join(target_obj.get('description', [])))

#     object_docs = [(nlp(' '.join(obj['title']) + ' ' + ' '.join(obj.get('description', []))), obj)
#                    for obj in object_list]

#     similarities = []
#     for doc, obj in object_docs:
#         phrase_similarity = get_similarity(target_doc, doc)

#         word_similarities = []
#         for target_token in target_doc:
#             target_token_synonyms = get_synonyms(target_token.text)
#             target_token_synonyms = get_synonyms(target_token.text.lower())
#             target_token_synonym_list = list(target_token_synonyms | {target_token.text.lower()})

#         for doc_token in doc:
#             doc_token_synonyms = get_synonyms(doc_token.text.lower())
#             token_similarities = [get_similarity(nlp(target_syn), nlp(doc_syn))
#                 for target_syn, doc_syn in
#                 combinations(target_token_synonym_list, 2)
#                 for doc_syn in doc_token_synonyms | {doc_token.text.lower()}]

#             max_similarity = max(
#                 token_similarities) if token_similarities else 0
#             word_similarities.append(max_similarity)


#         word_similarity = sum(word_similarities) / len(word_similarities) if word_similarities else 0
#         total_similarity = 0.7 * phrase_similarity + 0.3 * word_similarity
#         similarities.append((obj, total_similarity))

#     similarities.sort(key=lambda x: -x[1])
#     recommended_objs = [obj for obj, sim in similarities[:2]]
#     return recommended_objs


if __name__ == "__main__":
    app.run()